from coches import Coche
import unittest

#estructura del test
    #importamos el programa 
    #hacemos pruebas: 
        #1) que imprima los atributos correctos
        #2) que ponga el valor de la velocidad correcto cuando haya acelerado y frenado 


class TestCoche(unittest.TestCase):
    def test_atributos(self):
        coche1 = Coche("Rojo", "Ferrari", "250 GTO", "0000 BBB", 30)
        coche2 = Coche("Verde", "Tesla", "Modelo S", "1111 AAA", 50)

        atributos1 = coche1.ImprimirAtributos()
        atributos2 = coche2.ImprimirAtributos()

        self.assertEqual(atributos1, None)
        self.assertEqual(atributos2, None)



    def test_acelerar(self):
        coche1 = Coche("Rojo", "Ferrari", "250 GTO", "0000 BBB", 30)
        coche2 = Coche("Verde", "Tesla", "Modelo S", "1111 AAA", 50)

        acelerar1 = coche1.Acelerar()
        acelerar2 = coche2.Acelerar()

        self.assertEqual(acelerar1, 60)
        self.assertEqual(acelerar2, 80)



    def test_frenar(self):
        coche1 = Coche("Rojo", "Ferrari", "250 GTO", "0000 BBB", 30)
        coche2 = Coche("Verde", "Tesla", "Modelo S", "1111 AAA", 50)

        frenar1 = coche1.Frenar()
        frenar2 = coche2.Frenar()

        self.assertEqual(frenar1, 10)
        self.assertEqual(frenar2, 30)


if __name__ == "__main__":
    unittest.main()