class Coche:
    """Una clase que nos dice los atributos de un coche"""
    #definimos el constructor de la clase
    def __init__ (self, c, m, mo, mat, v):
        self. color = c
        self.marca = m
        self.modelo = mo 
        self.matricula = mat
        self.velocidad = v 

    def ImprimirAtributos (self):
        print ("Tu coche es de color {c}, su marca es {m} y modelo {mo}. Tiene matrícula {mat}".format(c=self.color, m=self.marca, mo=self.modelo, mat=self.matricula))

#creamos los métodos de acelerar y frenar
    #si el coche acelera, debemos sumarle a la velocidad dicho incremento
    def Acelerar (self):
        return self.velocidad + 30

    #si el coche frena, debemos restarle a la velocidad dicha disminución
    def Frenar (self):
        return self.velocidad - 20

#establecemos los atributos de los coches
coche1 = Coche("Rojo", "Ferrari", "250 GTO", "0000 BBB", 30)
coche2 = Coche("Verde", "Tesla", "Modelo S", "1111 AAA", 50)

#guardamos ambos coches en una lista
lista_coches = [coche1, coche2]

for coche in lista_coches:
    coche.ImprimirAtributos ()
    print ("Tu coche acelera hasta llegar a una velocidad de: " + str(coche.Acelerar()) + " metros por segundo")
    print ("Tu coche frena hasta llegar a una velocidad de: " + str(coche.Frenar()) + " metros por segundo")